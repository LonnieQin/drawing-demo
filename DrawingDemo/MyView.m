//
//  MyView.m
//  DrawingDemo
//
//  Created by amttgroup on 15-5-19.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//

#import "MyView.h"

@implementation MyView

- (UIImage*) reverseImageForText:(NSString*) text
{
    const size_t kImageWidth = 200;
    const size_t kImageHeight = 200;
    CGImageRef textImage = NULL;
    UIFont * font = [UIFont preferredFontForTextStyle:UIFontTextStyleHeadline];
    UIColor * color = [UIColor redColor];
    
    UIGraphicsBeginImageContext(CGSizeMake(kImageWidth, kImageHeight));
    [text drawInRect:CGRectMake(0, 0, kImageWidth, kImageHeight) withAttributes:@{NSFontAttributeName:font,NSForegroundColorAttributeName:color}];
    textImage = UIGraphicsGetImageFromCurrentImageContext().CGImage;
    UIGraphicsEndImageContext();
    
    return [UIImage imageWithCGImage:textImage scale:1.0 orientation:UIImageOrientationUp];
}

- (void) drawRect:(CGRect)rect
{
    [[UIColor colorWithRed:0 green:0 blue:1 alpha:0.1] set];
    [[self reverseImageForText:@"Hello World"] drawAtPoint:CGPointMake(50, 150)];
    UIRectFillUsingBlendMode(CGRectMake(100, 100, 100, 100), kCGBlendModeNormal);
}
@end
