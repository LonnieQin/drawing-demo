//
//  BlurryTextViewController.m
//  DrawingDemo
//
//  Created by amttgroup on 15-5-19.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//

#import "BlurryTextViewController.h"

@implementation BlurryTextViewController
- (IBAction)toggleBlur:(id)sender {
    CGRect frame = self.label.frame;
    if (frame.origin.x == floor(frame.origin.x)) {
        frame.origin.x += .25;
    } else {
        frame.origin.x = floorf(frame.origin.x);
    }
    self.label.frame = frame;
}

@end
