//
//  Menu1ViewController.m
//  DrawingDemo
//
//  Created by amttgroup on 15-5-19.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//

#import "Menu1ViewController.h"

@implementation Menu1ViewController


- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    UITableViewCell * cell = sender;
    UIViewController * controller = segue.destinationViewController;
    controller.title = cell.textLabel.text;
}
@end
