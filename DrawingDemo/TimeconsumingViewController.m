//
//  TimeconsumingViewController.m
//  DrawingDemo
//
//  Created by amttgroup on 15-5-19.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//

#import "TimeconsumingViewController.h"

@implementation TimeconsumingViewController

- (void) sometingTimeConsuming
{
    [NSThread sleepForTimeInterval:5];
}
- (IBAction)wastTime:(id)sender {
    [sender setEnabled:NO];
    [self.activity startAnimating];
    
    dispatch_queue_t bgQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
    
    dispatch_async(bgQueue, ^{
        [self sometingTimeConsuming];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.activity stopAnimating];
            [sender setEnabled:YES];
        });
        
    });
    
}

@end
