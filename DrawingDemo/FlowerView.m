//
//  FlowerView.m
//  DrawingDemo
//
//  Created by amttgroup on 15-5-19.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//

#import "FlowerView.h"

@implementation FlowerView
- (void) awakeFromNib
{
    NSLog(@"%s",__func__);
    self.contentMode = UIViewContentModeRedraw;
}

- (void) drawRect:(CGRect)rect
{
    CGSize size = self.bounds.size;
    CGFloat margin = 10;
    CGFloat radius = rintf(MIN(size.height-margin, size.width-margin) / 4);
    
    CGFloat xOffset,yOffset;
    CGFloat offset = rintf((size.height - size.width) / 2);
    if (offset > 0) {
        xOffset = (CGFloat)rint(margin/2);
        yOffset = offset;
    } else {
        xOffset = -offset;
        yOffset = rint(margin/2);
    }
    
    [[UIColor redColor] setFill];
    
    UIBezierPath * path = [UIBezierPath bezierPath];
    [path addArcWithCenter:CGPointMake(radius*2+xOffset, radius+yOffset) radius:radius startAngle:(CGFloat)-M_PI endAngle:0 clockwise:YES];
    [path addArcWithCenter:CGPointMake(radius*3+xOffset, radius*2+yOffset) radius:radius startAngle:(CGFloat)-M_PI_2 endAngle:(CGFloat)M_PI_2 clockwise:YES];
    [path addArcWithCenter:CGPointMake(radius*2+xOffset, radius*3+yOffset) radius:radius startAngle:0 endAngle:(CGFloat)M_PI clockwise:YES];
    [path addArcWithCenter:CGPointMake(radius+xOffset, radius*2+yOffset) radius:radius startAngle:(CGFloat)M_PI_2 endAngle:(CGFloat)-M_PI_2 clockwise:YES];
    [path closePath];
    [path fill];
}
@end
